## Padrão Abstract Factory

### Organização do projeto

```
exercicios/
├── src/
│   ├── ex1/
│   │   ├── mod.rs
│   │   └── vehicle_factory.rs
│   ├── ex2/
│   │   ├── gui_factories.rs
│   │   ├── gui_models.rs
│   │   └── mod.rs
│   ├── ex3/
│   │   ├── construction_factories.rs
│   │   ├── construction_models.rs
│   │   └── mod.rs
│   └── main.rs

```
Ou clique diretamente no link para cada implementação abaixo:

- [vehicle_factory.rs](exercicios/src/ex1/vehicle_factory.rs)
- [gui_factories.rs](exercicios/src/ex2/gui_factories.rs)
- [gui_models.rs](exercicios/src/ex2/gui_models.rs)
- [construction_factories.rs](exercicios/src/ex3/construction_factories.rs)
- [construction_models.rs](exercicios/src/ex3/construction_models.rs)
- [main.rs](exercicios/src/main.rs)

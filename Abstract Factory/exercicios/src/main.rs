mod exe1;
use crate::exe1::vehicle_factory::*;
mod exe2;
use crate::exe2::gui_factories::*;
mod exe3;
use crate::exe3::construction_factories::*;



fn main() {
    //exercicio 1
    println!();
    let electric_factory = ElectricVehicleFactory;
    let fuel_factory = FuelVehicleFactory;

    let car1 = electric_factory.produce_car();
    let motorcycle1 = electric_factory.produce_motorcycle();
    let truck1 = electric_factory.produce_truck();

    let car2 = fuel_factory.produce_car();
    let motorcycle2 = fuel_factory.produce_motorcycle();
    let truck2 = fuel_factory.produce_truck();

    car1.manufacture();
    motorcycle1.manufacture();
    truck1.manufacture();

    car2.manufacture();
    motorcycle2.manufacture();
    truck2.manufacture();

    //exercicio 2
    println!();
    let modern_factory = ModernWidgetFactory;
    let classic_factory = ClassicWidgetFactory;

    let button = modern_factory.create_button();
    let text_box = classic_factory.create_text_box();
    let menu = modern_factory.create_menu();

    button.render();
    text_box.render();
    menu.render();

    //exercicio 3
    println!();
    let contemporary_factory = ContemporaryHouseFactory;
    let colonial_factory = ColonialHouseFactory;

    let foundation = contemporary_factory.create_foundation();
    let walls = colonial_factory.create_walls();
    let roof = contemporary_factory.create_roof();

    foundation.build();
    walls.build();
    roof.build();

}



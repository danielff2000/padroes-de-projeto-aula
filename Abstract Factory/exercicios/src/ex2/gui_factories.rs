use crate::exe2::gui_models::*;

pub trait WidgetFactory {
    fn create_button(&self) -> Box<dyn Button>;
    fn create_text_box(&self) -> Box<dyn TextBox>;
    fn create_menu(&self) -> Box<dyn Menu>;
}


pub struct ModernWidgetFactory;
impl WidgetFactory for ModernWidgetFactory {
    fn create_button(&self) -> Box<dyn Button> {
        Box::new(ModernButton)
    }

    fn create_text_box(&self) -> Box<dyn TextBox> {
        Box::new(ModernTextBox)
    }

    fn create_menu(&self) -> Box<dyn Menu> {
        Box::new(ModernMenu)
    }
}


pub struct ClassicWidgetFactory;
impl WidgetFactory for ClassicWidgetFactory {
    fn create_button(&self) -> Box<dyn Button> {
        Box::new(ClassicButton)
    }

    fn create_text_box(&self) -> Box<dyn TextBox> {
        Box::new(ClassicTextBox)
    }

    fn create_menu(&self) -> Box<dyn Menu> {
        Box::new(ClassicMenu)
    }
}

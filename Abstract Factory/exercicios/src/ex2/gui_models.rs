pub trait Button {
    fn render(&self);
}

pub trait TextBox {
    fn render(&self);
}

pub trait Menu {
    fn render(&self);
}

pub struct ModernButton;
impl Button for ModernButton {
    fn render(&self) {
        println!("Renderizando um botão moderno.");
    }
}

pub struct ModernTextBox;
impl TextBox for ModernTextBox {
    fn render(&self) {
        println!("Renderizando uma caixa de texto moderna.");
    }
}

pub struct ModernMenu;
impl Menu for ModernMenu {
    fn render(&self) {
        println!("Renderizando um menu moderno.");
    }
}

pub struct ClassicButton;
impl Button for ClassicButton {
    fn render(&self) {
        println!("Renderizando um botão clássico.");
    }
}

pub struct ClassicTextBox;
impl TextBox for ClassicTextBox {
    fn render(&self) {
        println!("Renderizando uma caixa de texto clássica.");
    }
}

pub struct ClassicMenu;
impl Menu for ClassicMenu {
    fn render(&self) {
        println!("Renderizando um menu clássico.");
    }
}

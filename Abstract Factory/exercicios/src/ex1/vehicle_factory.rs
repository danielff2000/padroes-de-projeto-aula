
pub trait Vehicle {
    fn manufacture(&self);
}


pub trait VehicleFactory {
    fn produce_car(&self) -> Box<dyn Vehicle>;
    fn produce_motorcycle(&self) -> Box<dyn Vehicle>;
    fn produce_truck(&self) -> Box<dyn Vehicle>;
}

pub struct Car;
impl Vehicle for Car {
    fn manufacture(&self) {
        println!("Produzindo um carro.");
    }
}

pub struct Motorcycle;
impl Vehicle for Motorcycle {
    fn manufacture(&self) {
        println!("Produzindo uma motocicleta.");
    }
}

pub struct Truck;
impl Vehicle for Truck {
    fn manufacture(&self) {
        println!("Produzindo um caminhão.");
    }
}

pub struct ElectricVehicleFactory;
impl VehicleFactory for ElectricVehicleFactory {
    fn produce_car(&self) -> Box<dyn Vehicle> {
        Box::new(Car)
    }

    fn produce_motorcycle(&self) -> Box<dyn Vehicle> {
        Box::new(Motorcycle)
    }

    fn produce_truck(&self) -> Box<dyn Vehicle> {
        Box::new(Truck)
    }
}

pub struct FuelVehicleFactory;
impl VehicleFactory for FuelVehicleFactory {
    fn produce_car(&self) -> Box<dyn Vehicle> {
        Box::new(Car)
    }

    fn produce_motorcycle(&self) -> Box<dyn Vehicle> {
        Box::new(Motorcycle)
    }

    fn produce_truck(&self) -> Box<dyn Vehicle> {
        Box::new(Truck)
    }
}
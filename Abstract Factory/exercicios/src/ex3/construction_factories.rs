
use crate::exe3::construction_models::*;


pub trait HouseFactory {
    fn create_foundation(&self) -> Box<dyn Foundation>;
    fn create_walls(&self) -> Box<dyn Walls>;
    fn create_roof(&self) -> Box<dyn Roof>;
}



pub struct ContemporaryHouseFactory;
impl HouseFactory for ContemporaryHouseFactory {
    fn create_foundation(&self) -> Box<dyn Foundation> {
        Box::new(ContemporaryFoundation)
    }

    fn create_walls(&self) -> Box<dyn Walls> {
        Box::new(ContemporaryWalls)
    }

    fn create_roof(&self) -> Box<dyn Roof> {
        Box::new(ContemporaryRoof)
    }
}


pub struct ColonialHouseFactory;
impl HouseFactory for ColonialHouseFactory {
    fn create_foundation(&self) -> Box<dyn Foundation> {
        Box::new(ColonialFoundation)
    }

    fn create_walls(&self) -> Box<dyn Walls> {
        Box::new(ColonialWalls)
    }

    fn create_roof(&self) -> Box<dyn Roof> {
        Box::new(ColonialRoof)
    }
}

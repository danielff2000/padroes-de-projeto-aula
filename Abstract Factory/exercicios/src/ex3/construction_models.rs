// Traits representando as partes de uma casa
pub trait Foundation {
    fn build(&self);
}

pub trait Walls {
    fn build(&self);
}

pub trait Roof {
    fn build(&self);
}

// Implementações para um estilo arquitetônico contemporâneo
pub struct ContemporaryFoundation;
impl Foundation for ContemporaryFoundation {
    fn build(&self) {
        println!("Construindo uma fundação contemporânea.");
    }
}

pub struct ContemporaryWalls;
impl Walls for ContemporaryWalls {
    fn build(&self) {
        println!("Construindo paredes contemporâneas.");
    }
}

pub struct ContemporaryRoof;
impl Roof for ContemporaryRoof {
    fn build(&self) {
        println!("Construindo um telhado contemporâneo.");
    }
}

// Implementações para um estilo arquitetônico colonial
pub struct ColonialFoundation;
impl Foundation for ColonialFoundation {
    fn build(&self) {
        println!("Construindo uma fundação colonial.");
    }
}

pub struct ColonialWalls;
impl Walls for ColonialWalls {
    fn build(&self) {
        println!("Construindo paredes coloniais.");
    }
}

pub struct ColonialRoof;
impl Roof for ColonialRoof {
    fn build(&self) {
        println!("Construindo um telhado colonial.");
    }
}


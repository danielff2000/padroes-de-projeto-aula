## Padrão Factory Method

```
exercicios/
├── src/
│   ├── ex1/
│   │   ├── document_factory.rs
│   │   └── mod.rs
│   ├── ex2/
│   │   ├── game_factories.rs
│   │   └── mod.rs
│   ├── ex3/
│   │   ├── reader_factories.rs
│   │   └── mod.rs
│   ├── ex4/
│   │   ├── event_factory.rs
│   │   └── mod.rs
│   ├── ex5/
│   │    ├── mod.rs
│   │    └── report_factory.rs
│   └── main.rs

```

ou clique diretamente no link para cada implementação abaixo:
- [Document Factory](exercicios/src/ex1/document_factory.rs)
- [Game Factories](exercicios/src/ex2/game_factories.rs)
- [Reader Factories](exercicios/src/ex3/reader_factories.rs)
- [Event Factory](exercicios/src/ex4/event_factory.rs)
- [Report Factory](exercicios/src/ex5/report_factory.rs)
- [Main](exercicios/src/main.rs)
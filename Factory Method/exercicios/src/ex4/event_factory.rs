pub trait Event {
    fn message(&self) -> String;
}

pub struct ErrorEvent {
    pub msg: String,
}

impl Event for ErrorEvent {
    fn message(&self) -> String {
        format!("Erro: {}", self.msg)
    }
}

pub struct WarningEvent {
    pub msg: String,
}

impl Event for WarningEvent {
    fn message(&self) -> String {
        format!("Aviso: {}", self.msg)
    }
}

pub struct InfoEvent {
    pub msg: String,
}

impl Event for InfoEvent {
    fn message(&self) -> String {
        format!("Informativo: {}", self.msg)
    }
}

pub enum EventType {
    Error,
    Warning,
    Info,
}

pub struct EventFactory;

impl EventFactory {
    pub fn create_event(event_type: EventType, msg: String) -> Box<dyn Event> {
        match event_type {
            EventType::Error => Box::new(ErrorEvent { msg }),
            EventType::Warning => Box::new(WarningEvent { msg }),
            EventType::Info => Box::new(InfoEvent { msg }),
        }
    }
}


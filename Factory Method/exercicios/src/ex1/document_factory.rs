pub trait Document {
    fn load(&self) -> String;
}

pub struct PDF;
impl Document for PDF {
    fn load(&self) -> String {
        String::from("Carregando documento PDF...")
    }
}
pub struct Word;
impl Document for Word {
    fn load(&self) -> String {
        String::from("Carregando documento Word...")
    }
}
pub struct HTML;
impl Document for HTML {
    fn load(&self) -> String {
        String::from("Carregando documento HTML...")
    }
}

pub enum DocumentType {
    Pdf,
    Word,
    Html,
}

pub struct DocumentFactory;

impl DocumentFactory {
    pub fn create_document(doc_type: DocumentType) -> Box<dyn Document> {
        match doc_type {
            DocumentType::Pdf => Box::new(PDF),
            DocumentType::Word => Box::new(Word),
            DocumentType::Html => Box::new(HTML),
        }
    }
}

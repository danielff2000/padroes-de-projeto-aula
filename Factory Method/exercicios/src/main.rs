mod ex1;
mod ex2;
mod ex3;
mod ex4;
mod ex5;

use crate::ex1::document_factory::*;
use crate::ex2::game_factories::*;
use crate::ex3::reader_factories::*;
use crate::ex4::event_factory::*;
use crate::ex5::report_factory::*;



fn main() {
    //ex1
    let pdf_document = DocumentFactory::create_document(DocumentType::Pdf);
    println!("{}", pdf_document.load());

    let word_document = DocumentFactory::create_document(DocumentType::Word);
    println!("{}", word_document.load());

    let html_document = DocumentFactory::create_document(DocumentType::Html);
    println!("{}", html_document.load());

    //ex2
    let enemy = EnemyFactory::create_enemy(EnemyType::Soldier);
    enemy.attack();

    let big_boss = EnemyFactory::create_enemy(EnemyType::Boss);
    big_boss.attack();

    //ex3
    let json_reader = ConfigReaderFactory::create_reader(ConfigType::Json);
    println!("{}", json_reader.read("config.json"));

    let xml_reader = ConfigReaderFactory::create_reader(ConfigType::Xml);
    println!("{}", xml_reader.read("config.xml"));

    let yaml_reader = ConfigReaderFactory::create_reader(ConfigType::Yaml);
    println!("{}", yaml_reader.read("config.yaml"));

    //ex4
    let error_event = EventFactory::create_event(EventType::Error, "Falha no sistema".to_string());
    println!("{}", error_event.message());

    let warning_event = EventFactory::create_event(EventType::Warning, "Espaço em disco baixo".to_string());
    println!("{}", warning_event.message());

    let info_event = EventFactory::create_event(EventType::Info, "Processo concluído com sucesso".to_string());
    println!("{}", info_event.message());

    //ex5
    let pdf_report = ReportFactory::create_report(ReportType::Pdf);
    println!("{}", pdf_report.generate());

    let csv_report = ReportFactory::create_report(ReportType::Csv);
    println!("{}", csv_report.generate());

    let html_report = ReportFactory::create_report(ReportType::Html);
    println!("{}", html_report.generate());

}

pub trait Report {
    fn generate(&self) -> String;
}
pub struct PdfReport;
impl Report for PdfReport {
    fn generate(&self) -> String {
        String::from("Relatório PDF gerado.")
    }
}

pub struct CsvReport;
impl Report for CsvReport {
    fn generate(&self) -> String {
        String::from("Relatório CSV gerado.")
    }
}

pub struct HtmlReport;
impl Report for HtmlReport {
    fn generate(&self) -> String {
        String::from("Relatório HTML gerado.")
    }
}


pub enum ReportType {
    Pdf,
    Csv,
    Html,
}

pub struct ReportFactory;

impl ReportFactory {
    pub fn create_report(report_type: ReportType) -> Box<dyn Report> {
        match report_type {
            ReportType::Pdf => Box::new(PdfReport),
            ReportType::Csv => Box::new(CsvReport),
            ReportType::Html => Box::new(HtmlReport),
        }
    }
}

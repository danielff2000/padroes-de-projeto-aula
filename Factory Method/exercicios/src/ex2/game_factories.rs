pub trait Enemy {
    fn attack(&self);
}

pub struct Soldier;
impl Enemy for Soldier {
    fn attack(&self) {
        println!("O soldado ataca!");
    }
}

pub struct Monster;
impl Enemy for Monster {
    fn attack(&self) {
        println!("O monstro ataca!");
    }
}

pub struct Boss;
impl Enemy for Boss {
    fn attack(&self) {
        println!("O chefe ataca com força!");
    }
}

pub enum EnemyType {
    Soldier,
    Monster,
    Boss,
}

pub struct EnemyFactory;

impl EnemyFactory {
    pub fn create_enemy(enemy_type: EnemyType) -> Box<dyn Enemy> {
        match enemy_type {
            EnemyType::Soldier => Box::new(Soldier),
            EnemyType::Monster => Box::new(Monster),
            EnemyType::Boss => Box::new(Boss),
        }
    }
}


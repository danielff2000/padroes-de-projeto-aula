pub trait ConfigReader {
    fn read(&self, path: &str) -> String;
}

pub struct JsonConfigReader;
impl ConfigReader for JsonConfigReader {
    fn read(&self, path: &str) -> String {
        format!("Lendo configuração JSON do arquivo: {}", path)
    }
}

pub struct XmlConfigReader;
impl ConfigReader for XmlConfigReader {
    fn read(&self, path: &str) -> String {
        format!("Lendo configuração XML do arquivo: {}", path)
    }
}

pub struct YamlConfigReader;
impl ConfigReader for YamlConfigReader {
    fn read(&self, path: &str) -> String {
        format!("Lendo configuração YAML do arquivo: {}", path)
    }
}


pub enum ConfigType {
    Json,
    Xml,
    Yaml,
}


pub struct ConfigReaderFactory;

impl ConfigReaderFactory {
    pub fn create_reader(config_type: ConfigType) -> Box<dyn ConfigReader> {
        match config_type {
            ConfigType::Json => Box::new(JsonConfigReader),
            ConfigType::Xml => Box::new(XmlConfigReader),
            ConfigType::Yaml => Box::new(YamlConfigReader),
        }
    }
}

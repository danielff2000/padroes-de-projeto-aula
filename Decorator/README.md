## Padrão Decorator

### Organização do projeto
```
exercicios/
├── src/
│ ├── modules/
│ │ ├── button_decorator.rs
│ │ ├── coffee_decorator.rs
│ │ ├── form_decorator.rs
│ │ ├── shape_decorator.rs
│ │ ├── text_decorator.rs
│ │ └── mod.rs
│ └── main.rs

```
ou clique diretamente no link para cada implementação abaixo: 
* [Coffe Decorator](exercicios/src/modules/coffee_decorator.rs)
* [Button Decorator](exercicios/src/modules/button_decorator.rs)
* [Shape Decorator](exercicios/src/modules/shape_decorator.rs)
* [Text Decorator](exercicios/src/modules/text_decorator.rs)
* [Form Decorator](exercicios/src/modules/form_decorator.rs)     
* [Main](exercicios/src/main.rs)
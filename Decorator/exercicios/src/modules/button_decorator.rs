pub trait Button {
    fn render(&self) -> String;
}

pub struct BasicButton;

impl Button for BasicButton {
    fn render(&self) -> String {
        "Botão".to_string()
    }
}

pub trait ButtonDecorator: Button {
    fn new(base: Box<dyn Button>) -> Self;
}

pub struct BorderDecorator {
    button: Box<dyn Button>,
    border_style: String,
}

impl Button for BorderDecorator {
    fn render(&self) -> String {
        format!("{} com {} borda", self.button.render(), self.border_style)
    }
}

impl BorderDecorator {
    pub fn new(base: Box<dyn Button>, border_style: &str) -> Self {
        Self {
            button: base,
            border_style: border_style.to_string(),
        }
    }
}

pub struct DashedBorderDecorator {
    button: Box<dyn Button>,
}

impl Button for DashedBorderDecorator {
    fn render(&self) -> String {
        format!("{} com borda tracejada", self.button.render())
    }
}

impl ButtonDecorator for DashedBorderDecorator {
    fn new(base: Box<dyn Button>) -> Self {
        Self { button: base }
    }
}


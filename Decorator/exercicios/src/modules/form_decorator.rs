pub trait Form {
    fn render(&self) -> String;
}

pub struct BasicForm;

impl Form for BasicForm {
    fn render(&self) -> String {
        "Forma".to_string()
    }
}

pub trait FormDecorator: Form {
    fn new(base: Box<dyn Form>) -> Self;
}

pub struct FieldDecorator {
    form: Box<dyn Form>,
    field: String,
}

impl Form for FieldDecorator {
    fn render(&self) -> String {
        format!("{} com campo de seleção{}", self.form.render(), self.field)
    }
}

impl FieldDecorator {
    pub fn new(base: Box<dyn Form>, field: &str) -> Self {
        Self {
            form: base,
            field: field.to_string(),
        }
    }
}

pub struct TextFieldDecorator {
    form: Box<dyn Form>,
    field: String,
}

impl Form for TextFieldDecorator {
    fn render(&self) -> String {
        format!("{} com campo de texto '{}'", self.form.render(), self.field)
    }
}

impl TextFieldDecorator {
    pub fn new(base: Box<dyn Form>, field: &str) -> Self {
        Self {
            form: base,
            field: field.to_string(),
        }
    }
}

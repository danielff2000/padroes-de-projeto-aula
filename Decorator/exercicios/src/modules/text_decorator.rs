pub trait Text {
    fn render(&self) -> String;
}

pub struct BasicText {
    content: String,
}

impl BasicText {
    pub fn new(content: &str) -> Self {
        Self {
            content: content.to_string(),
        }
    }
}

impl Text for BasicText {
    fn render(&self) -> String {
        self.content.clone()
    }
}

pub trait TextDecorator: Text {
    fn new(base: Box<dyn Text>) -> Self;
}

pub struct BoldDecorator {
    text: Box<dyn Text>,
}

impl Text for BoldDecorator {
    fn render(&self) -> String {
        format!("<b>{}</b>", self.text.render())
    }
}

impl TextDecorator for BoldDecorator {
    fn new(base: Box<dyn Text>) -> Self {
        Self { text: base }
    }
}


pub struct ItalicDecorator {
    text: Box<dyn Text>,
}

impl Text for ItalicDecorator {
    fn render(&self) -> String {
        format!("<i>{}</i>", self.text.render())
    }
}

impl TextDecorator for ItalicDecorator {
    fn new(base: Box<dyn Text>) -> Self {
        Self { text: base }
    }
}

pub struct UnderlineDecorator {
    text: Box<dyn Text>,
}

impl Text for UnderlineDecorator {
    fn render(&self) -> String {
        format!("<u>{}</u>", self.text.render())
    }
}

impl TextDecorator for UnderlineDecorator {
    fn new(base: Box<dyn Text>) -> Self {
        Self { text: base }
    }
}

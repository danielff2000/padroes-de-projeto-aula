pub trait Coffee {
    fn prepare(&self) -> String;
}

pub struct SimpleCoffee;

impl Coffee for SimpleCoffee {
    fn prepare(&self) -> String {
        "Café".to_string()
    }
}

pub trait CoffeeDecorator: Coffee {
    fn new(base: Box<dyn Coffee>) -> Self;
}

pub struct MilkDecorator {
    coffee: Box<dyn Coffee>,
}

impl Coffee for MilkDecorator {
    fn prepare(&self) -> String {
        format!("{}, com leite", self.coffee.prepare())
    }
}

impl CoffeeDecorator for MilkDecorator {
    fn new(base: Box<dyn Coffee>) -> Self {
        Self { coffee: base }
    }
}

pub struct SugarDecorator {
    coffee: Box<dyn Coffee>,
}

impl Coffee for SugarDecorator {
    fn prepare(&self) -> String {
        format!("{}, com açucar", self.coffee.prepare())
    }
}

impl CoffeeDecorator for SugarDecorator {
    fn new(base: Box<dyn Coffee>) -> Self {
        Self { coffee: base }
    }
}

pub struct ChocolateDecorator {
    coffee: Box<dyn Coffee>,
}

impl Coffee for ChocolateDecorator {
    fn prepare(&self) -> String {
        format!("{}, com chocolate", self.coffee.prepare())
    }
}

impl CoffeeDecorator for ChocolateDecorator {
    fn new(base: Box<dyn Coffee>) -> Self {
        Self { coffee: base }
    }
}
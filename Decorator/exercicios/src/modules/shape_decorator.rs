pub trait Shape {
    fn render(&self) -> String;
}

pub struct Circle;

impl Shape for Circle {
    fn render(&self) -> String {
        "Circulo".to_string()
    }
}

pub trait ShapeDecorator: Shape {
    fn new(base: Box<dyn Shape>) -> Self;
}

pub struct ColorDecorator {
    shape: Box<dyn Shape>,
    color: String,
}

impl Shape for ColorDecorator {
    fn render(&self) -> String {
        format!("{} com cor {}", self.shape.render(), self.color)
    }
}

impl ColorDecorator {
    pub fn new(base: Box<dyn Shape>, color: &str) -> Self {
        Self {
            shape: base,
            color: color.to_string(),
        }
    }
}

pub struct RedDecorator {
    shape: Box<dyn Shape>,
}

impl Shape for RedDecorator {
    fn render(&self) -> String {
        format!("{} vermelho", self.shape.render())
    }
}

impl ShapeDecorator for RedDecorator {
    fn new(base: Box<dyn Shape>) -> Self {
        Self { shape: base }
    }
}


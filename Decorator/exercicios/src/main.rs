mod modules;
use crate::modules::coffee_decorator::*;
use crate::modules::text_decorator::*;
use crate::modules::button_decorator::*;
use crate::modules::form_decorator::*;
use crate::modules::shape_decorator::*;


fn main() {
    let basic_coffee = Box::new(SimpleCoffee);

    let coffee_with_milk = Box::new(MilkDecorator::new(basic_coffee));
    println!("Café com leite: {}", coffee_with_milk.prepare());

    let coffee_with_milk_and_sugar = Box::new(SugarDecorator::new(coffee_with_milk));
    println!("Café com leite e açucar: {}", coffee_with_milk_and_sugar.prepare());

    let basic_text = Box::new(BasicText::new("olá mundo"));
    println!("Texto básico: {}", basic_text.render());
    let underlined_text = Box::new(UnderlineDecorator::new(basic_text));
    println!("Texto sublinhado: {}", underlined_text.render());


}

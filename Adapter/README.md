## Padrão Adapter

### Organização do projeto

```
exercicio_aula/
├── src/
│ ├── modules/
│ │ ├── pilot.rs
│ │ ├── single_engine.rs
│ │ ├── twin_engine.rs
│ │ ├── twin_engine_adapter.rs
│ │ └── mod.rs
│ └── main.rs

```
ou clique diretamente no link para cada implementação abaixo: 
* [pilot](exercicio_aula/src/modules/pilot.rs)
* [single_engine](exercicio_aula/src/modules/single_engine.rs)
* [twin_engine](exercicio_aula/src/modules/twin_engine.rs)
* [twin_engine_adapter](exercicio_aula/src/modules/twin_engine_adapter.rs)
* [Main](exercicio_aula/src/main.rs)

```
extra/
├── src/
│ ├── modules/
│ │ ├── cdf_to_ldf_adapter.rs
│ │ ├── ldf_to_cdf_adapter.rs
│ │ ├── current_data_source.rs
│ │ ├── legacy_data_source.rs
│ │ ├── data_provider.rs
│ │ └── mod.rs
│ └── main.rs

```
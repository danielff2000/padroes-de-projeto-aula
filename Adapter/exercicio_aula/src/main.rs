mod modules;
use modules::twin_engine_adapter::TwinEngineAdapter;

use crate::modules::pilot::*;
use crate::modules::single_engine::*;
use crate::modules::twin_engine::*;

fn main() {
    let cessna = SingleEngineAircraft;
    let pilot = Pilot::new(&cessna);
    pilot.fly();

    let bimotor = TwinEnginePlane;
    let bimotor_adapter =TwinEngineAdapter::new(&bimotor);
    let best_pilot = Pilot::new(&bimotor_adapter);
    best_pilot.fly();
    
}

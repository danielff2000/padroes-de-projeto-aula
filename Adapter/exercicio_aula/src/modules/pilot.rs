use crate::Aircraft;

pub struct Pilot<'a, A: Aircraft> {
    aircraft: &'a A,
}

impl<'a, A: Aircraft> Pilot<'a, A> {
    pub fn new(aircraft: &'a A) -> Self {
        Pilot { aircraft }
    }

    pub fn fly(&self) {
        self.aircraft.start_engine();
        self.aircraft.take_off();
    }
}

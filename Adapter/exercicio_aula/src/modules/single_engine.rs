pub struct SingleEngineAircraft;

pub trait Aircraft {
    fn start_engine(&self);
    fn shutdown_engine(&self);
    fn take_off(&self);
}

impl Aircraft for SingleEngineAircraft {
    fn start_engine(&self) {
        println!("Monomotor ligando o motor...");
    }

    fn shutdown_engine(&self) {
        println!("Monomotor desligando o motor...");
    }

    fn take_off(&self) {
        println!("Monomotor decolando...");
    }


}
pub struct TwinEnginePlane;

pub trait TwinEngineAircraft {
    fn start_engine1(&self);
    fn start_engine2(&self);
    fn shutdown_engine1(&self);
    fn shutdown_engine2(&self);
    fn take_off(&self);
}

impl TwinEngineAircraft for TwinEnginePlane {
    fn start_engine1(&self) {
        println!("Bimotor ligando motor 1...");
    }
    fn start_engine2(&self) {
        println!("Bimotor ligando motor 2...");
    }

    fn shutdown_engine1(&self) {
        println!("Bimotor desligando o motor 1... ");
    }

    fn shutdown_engine2(&self) {
        println!("Bimotor desligando o motor 2... ");
    }

    fn take_off(&self) {
        println!("Bimotor decolando... ");
    }

}
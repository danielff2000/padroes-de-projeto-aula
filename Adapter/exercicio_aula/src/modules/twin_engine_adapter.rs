use crate::modules::twin_engine::*;
use crate::modules::single_engine::*;

pub struct TwinEngineAdapter<'a> {
    plane: &'a TwinEnginePlane
}

impl<'a> TwinEngineAdapter<'a> {
    pub fn new(twin_engine: &'a TwinEnginePlane) -> Self{
        TwinEngineAdapter{
            plane: twin_engine
        }
    }
}

impl<'a> Aircraft for TwinEngineAdapter<'a> {
    fn start_engine(&self) {
        self.plane.start_engine1();
        self.plane.start_engine2();
    }

    fn shutdown_engine(&self) {
        self.plane.shutdown_engine1();
        self.plane.shutdown_engine2();
    }

    fn take_off(&self) {
        self.plane.take_off();
    }


}
use crate::modules::data_provider::*;
use crate::modules::ldf_to_cdf_adapter::*;

pub struct CurrentDataSource;

impl DataProvider for CurrentDataSource {
    fn get_data(&self) -> String {
        String::from("
            obj: pokemon;
            nome: pikachu;
            tipo: eletrico;
         ")
    }
}


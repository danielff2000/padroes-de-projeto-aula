use regex::Regex;
use crate::modules::data_provider::*;

pub struct CurrentToLegacyAdapter<T: DataProvider> {
   pub current_provider: T,
}

impl<T: DataProvider> CurrentToLegacyAdapter<T> {
    pub fn new(current_provider: T) -> Self {
        CurrentToLegacyAdapter { current_provider }
    }

    pub fn convert_current_to_legacy(&self, current_input: &str) -> String {
        let re = Regex::new(r"(\w+): (.*?);").unwrap();
        re.captures_iter(current_input)
            .map(|cap| format!("[{}] {} [/{}]", &cap[1], cap[2].trim(), &cap[1]))
            .collect::<Vec<_>>()
            .join("\n")
    }
}
impl<T: DataProvider> DataProvider for CurrentToLegacyAdapter<T> {
    fn get_data(&self) -> String {
        let current_data = self.current_provider.get_data();
        self.convert_current_to_legacy(&current_data)
    }
}

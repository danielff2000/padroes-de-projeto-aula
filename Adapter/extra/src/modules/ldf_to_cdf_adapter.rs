use regex::Regex;
use crate::modules::data_provider::*;

pub struct LegacyToCurrentAdapter<T: DataProvider> {
    pub legacy_provider: T,
}

impl<T: DataProvider> LegacyToCurrentAdapter<T> {
    pub fn new(legacy_provider: T) -> Self {
        LegacyToCurrentAdapter { legacy_provider }
    }

    pub fn convert_legacy_to_current(&self, legacy_input: &str) -> String {
        let re = Regex::new(r"\[(\w+)] ([^\[\]]+) \[/\w+]").unwrap();
        re.captures_iter(legacy_input)
            .map(|cap| format!("{}: {};", &cap[1], cap[2].trim()))
            .collect::<Vec<_>>()
            .join("\n")
    }
}

impl<T: DataProvider> DataProvider for LegacyToCurrentAdapter<T> {
    fn get_data(&self) -> String {
        let legacy_data = self.legacy_provider.get_data();
        self.convert_legacy_to_current(&legacy_data)
    }
}

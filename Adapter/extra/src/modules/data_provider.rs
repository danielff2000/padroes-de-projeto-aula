pub trait DataProvider{
    fn get_data(&self)-> String;
}

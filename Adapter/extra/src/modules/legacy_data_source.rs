
use crate::{modules::data_provider::*, CurrentToLegacyAdapter};

pub struct LegacyDataSource;

impl DataProvider for LegacyDataSource {
    fn get_data(&self) -> String {
        String::from("
        [obj] pokemon [/obj]
        [nome] pikachu [/nome]
        [tipo] eletrico [/tipo]
        ")
    }
}




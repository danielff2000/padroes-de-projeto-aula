mod modules;
use crate::modules::data_provider::*; //import importante
use crate::modules::current_data_source::*;
use crate::modules::legacy_data_source::*;
use crate::modules::cdf_to_ldf_adapter::*;
use crate::modules::ldf_to_cdf_adapter::*;


fn main() {
 
    //mock da fonte de dados em ldf
    let legacy_data_format_provider = LegacyDataSource;
    let adapter_ldf = LegacyToCurrentAdapter::new(legacy_data_format_provider);
    //com o adapter podemos consumir dados do tipo ldf como se fosse cdf
    let result_ldf_adapter = adapter_ldf.get_data();
    println!("Dados convertidos para cdf:\n{}", result_ldf_adapter);

    //exemplo contrario
    println!();
    let current_data_format_provider = CurrentDataSource;
    let adapter_cdf = CurrentToLegacyAdapter::new(current_data_format_provider);
    let result_cdf_adapter = adapter_cdf.get_data();
    println!("Dados convertidos para ldf:\n{}", result_cdf_adapter);

}






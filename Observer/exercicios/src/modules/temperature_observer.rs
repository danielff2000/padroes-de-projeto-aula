use crate::modules::observer_base::*;


pub struct TemperatureObservable<'a, T: Observer> {
    observers: Vec<&'a T>,
    temperature: f32,
    meta_data: &'static str
}
impl<'a, T: Observer + PartialEq> TemperatureObservable<'a, T> {
    pub fn new(start_temp: f32) -> Self {
        TemperatureObservable {
            observers: Vec::new(),
            temperature: start_temp,
            meta_data: "Temperatura"
        }
    }
    pub fn set_temperature(&mut self, new_temperature: f32){
        self.temperature =  new_temperature;
    }

}
impl<'a, T: Observer + PartialEq> Subject<'a, T> for TemperatureObservable<'a, T>{
    fn subscribe(&mut self, observer: &'a T) {
        self.observers.push(observer);
    }
    fn unsubscribe(&mut self, observer: &'a T) {
        if let Some(idx) = self.observers.iter().position(|x| *x == observer) {
            self.observers.remove(idx);
        }
    }
    fn notify(&mut self) {
        for item in self.observers.iter() {
            item.update(self.meta_data,self.temperature);
        }
    }
}


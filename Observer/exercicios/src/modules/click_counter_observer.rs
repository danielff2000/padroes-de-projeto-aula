use crate::modules::observer_base::*;
//exercicio 1

pub struct ClickCounter<'a, T: Observer> {
    observers: Vec<&'a T>,
    count: u32,
    meta_data: &'static str
}
impl<'a, T: Observer + PartialEq> ClickCounter<'a, T> {
    pub fn new() -> Self {
        ClickCounter {
            observers: Vec::new(),
            count: 0,
            meta_data: "Contador"
        }
    }

}
impl<'a, T: Observer + PartialEq> Subject<'a, T> for ClickCounter<'a, T>{
    fn subscribe(&mut self, observer: &'a T) {
        self.observers.push(observer);
    }
    fn unsubscribe(&mut self, observer: &'a T) {
        if let Some(idx) = self.observers.iter().position(|x| *x == observer) {
            self.observers.remove(idx);
        }
    }
    fn notify(&mut self) {
        self.count += 1;
        for item in self.observers.iter() {
            item.update(self.meta_data,self.count);
        }
    }
}


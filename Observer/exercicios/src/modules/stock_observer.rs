use crate::modules::observer_base::*;

pub struct StockItem{
    name: String,
}

impl StockItem{
    pub fn new(name: &'static str) -> Self{
        StockItem{
            name : name.to_string()
        }
    }
}

pub struct StockObservable<'a, T: Observer> {
    observers: Vec<&'a T>,
    stock: Vec<&'a StockItem>,
    meta_data: &'static str
}
impl<'a, T: Observer + PartialEq> StockObservable<'a, T> {
    pub fn new() -> Self {
        StockObservable {
            observers: Vec::new(),
            stock: Vec::new(),
            meta_data: "Quantidade de produtos no estoque"
        }
    }

    pub fn add_item_in_stock(&mut self, item: &'a StockItem){
        self.stock.push(item);
    }

    fn stock_amount(&self) -> u32{
        self.stock.len() as u32
    }



}
impl<'a, T: Observer + PartialEq> Subject<'a, T> for StockObservable<'a, T>{
    fn subscribe(&mut self, observer: &'a T) {
        self.observers.push(observer);
    }
    fn unsubscribe(&mut self, observer: &'a T) {
        if let Some(idx) = self.observers.iter().position(|x| *x == observer) {
            self.observers.remove(idx);
        }
    }
    fn notify(&mut self) {
        for item in self.observers.iter() {
            item.update(self.meta_data,&self.stock_amount());
        }
    }
}


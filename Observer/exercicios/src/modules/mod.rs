
pub mod observer_base;
pub mod click_counter_observer;
pub mod temperature_observer;
pub mod news_observer;
pub mod stock_observer;
pub mod chat_observer;
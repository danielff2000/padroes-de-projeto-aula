use crate::modules::observer_base::*;

pub struct Message{
    sender: String,
    message_content: String
}

impl Message{
    pub fn new(sender: String, message_content: String) -> Self{
        Message{
           sender,
           message_content
        }
    }

    pub fn get_lifetime_reference<'a>(&'a self) -> &'a Message {
        self
    }
    
}

pub struct ChatObservable<'a, T: Observer> {
    observers: Vec<&'a T>,
    message_history: Vec<Message>,
    meta_data: &'static str
}
impl<'a, T: Observer + PartialEq> ChatObservable<'a, T> {
    pub fn new() -> Self {
        ChatObservable {
            observers: Vec::new(),
            message_history: Vec::new(),
            meta_data: "Nova mensagem de"
        }
    }

    pub fn send_message(&mut self,sender_name: String, message_content: String){
        self.message_history.push(Message::new(sender_name, message_content));
    }

}
impl<'a, T: Observer + PartialEq> Subject<'a, T> for ChatObservable<'a, T>{
    fn subscribe(&mut self, observer: &'a T) {
        self.observers.push(observer);
    }
    fn unsubscribe(&mut self, observer: &'a T) {
        if let Some(idx) = self.observers.iter().position(|x| *x == observer) {
            self.observers.remove(idx);
        }
    }
    fn notify(&mut self) {
        for item in self.observers.iter() {
            let msg = self.message_history.last().expect("mensagem nao encontrada");
            item.update(self.meta_data,msg.sender.to_string());
        }
    }
}


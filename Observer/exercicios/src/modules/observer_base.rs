use std::fmt::Display;

pub trait Observer {
    fn update<D: Display>(&self, meta_data: &'static str,data: D);
}

pub trait Subject<'a, T: Observer + PartialEq>  {
    fn subscribe(&mut self, observer: &'a T);
    fn unsubscribe(&mut self, observer: &'a T);
    fn notify(&mut self);
}

#[derive(PartialEq)]
pub struct ObserverDisplay{
    pub name: &'static str,
}

impl ObserverDisplay {
    pub fn new(name: &'static str) -> Self{
        ObserverDisplay{
            name
        }
    }
}

impl Observer for ObserverDisplay {
    fn update<D : Display>(&self,meta_data: &'static str, data: D) {
        println!("{}, {}: {}",self.name,meta_data ,data);
    }
}

use crate::modules::observer_base::*;


pub struct NewsObservable<'a, T: Observer> {
    observers: Vec<&'a T>,
    news_content: String,
    meta_data: &'static str
}
impl<'a, T: Observer + PartialEq> NewsObservable<'a, T> {
    pub fn new() -> Self {
        NewsObservable {
            observers: Vec::new(),
            news_content: String::new(),
            meta_data: "Noticia"
        }
    }
    pub fn post_news(&mut self,news: String){
        self.news_content = news;
    }


}
impl<'a, T: Observer + PartialEq> Subject<'a, T> for NewsObservable<'a, T>{
    fn subscribe(&mut self, observer: &'a T) {
        self.observers.push(observer);
    }
    fn unsubscribe(&mut self, observer: &'a T) {
        if let Some(idx) = self.observers.iter().position(|x| *x == observer) {
            self.observers.remove(idx);
        }
    }
    fn notify(&mut self) {
        for item in self.observers.iter() {
            item.update(self.meta_data,&self.news_content);
        }
    }
}


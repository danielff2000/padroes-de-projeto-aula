mod modules;
use crate::modules::observer_base::*;
use crate::modules::click_counter_observer::*;
use crate::modules::temperature_observer::*;
use crate::modules::news_observer::*;
use crate::modules::chat_observer::*;
use crate::modules::stock_observer::*;
use crate::modules::stock_observer::StockObservable;//o compilador me obrigou a importar novamente  



fn main() {
    let mut click_counter: ClickCounter<'_, ObserverDisplay> =
                                                ClickCounter::new();
    
    let observer_a = ObserverDisplay::new("observador_a");
    let observer_b = ObserverDisplay::new("observador_b");
    let observer_c = ObserverDisplay::new("observador_c");
    let observer_d = ObserverDisplay::new("observador_d");

    click_counter.subscribe(&observer_a);
    click_counter.subscribe(&observer_b);
    click_counter.notify();
    click_counter.notify();

    let mut term : TemperatureObservable<'_, ObserverDisplay> =                 
                                            TemperatureObservable::new(10.0);
  
    term.subscribe(&observer_c);
    term.subscribe(&observer_d);

    term.notify();
    term.set_temperature(20.0);
    term.notify();

    let mut news: NewsObservable<'_, ObserverDisplay> = NewsObservable::new();

    news.subscribe(&observer_a);
    news.subscribe(&observer_b);
    news.post_news("lorem ipsum blá blá blá ...".to_string());
    news.notify();

    let mut stock: StockObservable<'_, ObserverDisplay> =
                                                     StockObservable::new();
    
    let item1 = StockItem::new("banana");
    let item2 = StockItem::new("Maçã");
    let item3 = StockItem::new("Uva");

    stock.add_item_in_stock(&item1);
    stock.add_item_in_stock(&item2);
    stock.add_item_in_stock(&item3);

    stock.subscribe(&observer_a);
    stock.subscribe(&observer_b);
    
    stock.notify();

    let mut chat: ChatObservable<'_, ObserverDisplay> =
                                                ChatObservable::new();

    chat.subscribe(&observer_a);
    chat.subscribe(&observer_b);
    chat.subscribe(&observer_c);
    chat.subscribe(&observer_d);

    chat.send_message((&observer_a).name.to_string(), "bom dia grupo"
                                                            .to_string());

    chat.notify();
    
}

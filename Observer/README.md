## Padrão Observer

### Organização do projeto
```
exercicios/
├── src/
│ ├── modules/
│ │ ├── click_counter_observer.rs
│ │ ├── temperature_observer.rs
│ │ ├── stock_observer.rs
│ │ ├── chat_observer.rs
│ │ ├── news_observer.rs
│ │ ├── observer_base.rs
│ │ └── mod.rs
│ └── main.rs

```
ou clique diretamente no link para cada implementação abaixo: 

* [Click Counter](exercicios/src/modules/click_counter_observer.rs)    
* [Temperature Observer](exercicios/src/modules/temperature_observer.rs)
* [Stock Observer](exercicios/src/modules/stock_observer.rs)
* [Chat Observer](exercicios/src/modules/chat_observer.rs)
* [News Observer](exercicios/src/modules/news_observer.rs)
* [Observer Base](exercicios/src/modules/observer_base.rs)
* [Main](exercicios/src/main.rs)
# Exercícios Padrões de Projeto

Este repositório contém uma série de exercícios relacionados à disciplina de Padrões de Projeto. Os exercícios são implementados na linguagem de programação Rust.

Aluno: Daniel de Faria Ferreira


## Padrões Implementados

- [Observer](/Observer)
- [Decorator](/Decorator)
- [Adapter](/Adapter/)
- [Facade](/Facade/)
- [Abstract Factory](/Abstract%20Factory/)
- [Factory Method](/Factory%20Method/)


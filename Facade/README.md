## Padrão Facade

### Organização do projeto

```
exercicios/
├── src/
│ ├── modules/
│ │ ├── connection.rs
│ │ ├── database.rs
│ │ ├── model.rs
│ │ ├── facade.rs
│ │ └── mod.rs
│ └── main.rs

```

ou clique diretamente no link para cada implementação abaixo: 
* [connection](exercicios/src/modules/connection.rs)
* [database](exercicios/src/modules/database.rs)
* [model](exercicios/src/modules/model.rs)
* [facade](exercicios/src/modules/facade.rs)    
* [Main](exercicios/src/main.rs)
pub struct Connection;

impl Connection {
    pub fn new() -> Self {
        Connection {}
    }

    pub fn establish(&self) {
        println!("Conexão estabelecida com o banco de dados.✅");
    }

    pub fn close(&self) {
        println!("Conexão encerrada com banco de dados.❌");
    }
}

pub struct Model {
    pub identifier: String,
    pub data: String,
}

impl Model {
    pub fn new(identifier: String, data: String) -> Self {
        Model { identifier, data }
    }
}

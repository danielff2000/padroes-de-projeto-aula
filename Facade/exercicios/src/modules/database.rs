use crate::modules::model::*;
pub struct Database;

impl Database {
   pub  fn new() -> Self {
        Database {}
    }

    pub fn execute_operation(&self, operation: &str, model: &Model) {
        println!("Operação: {}, ID: {}, dado: {}", operation, model.identifier, model.data);
    }
}

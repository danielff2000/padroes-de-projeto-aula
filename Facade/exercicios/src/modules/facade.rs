use crate::modules::database::*;
use crate::modules::connection::*;
use crate::modules::model::*;

pub struct Facade {
    pub database: Database,
    pub connection: Connection,
}

impl Facade {
    pub fn new() -> Self {
        Facade { 
            database: Database::new(),
            connection: Connection::new(),
        }
    }

    pub fn register(&self, model: &Model) {
        self.connection.establish();
        self.database.execute_operation("registrar", model);
        self.connection.close();
    }

    pub fn retrieve(&self, identifier: &str) {
        let model = Model::new(identifier.to_string(), String::new());
        self.connection.establish();
        self.database.execute_operation("recuperar", &model);
        self.connection.close();
    }

    pub fn update(&self, model: &Model) {
        self.connection.establish();
        self.database.execute_operation("atualizar", model);
        self.connection.close();
    }

    pub fn remove(&self, identifier: &str) {
        let model = Model::new(identifier.to_string(), String::new());
        self.connection.establish();
        self.database.execute_operation("remover", &model);
        self.connection.close();
    }
}
mod modules;
use crate::modules::connection::*;
use crate::modules::model::*;
use crate::modules::facade::*;

fn main() {
    let facade = Facade::new();

    let new_model = Model::new("1".to_string(), "novo usuario".to_string());
    facade.register(&new_model);

    facade.retrieve("1");

    let updated_model = Model::new("1".to_string(), "usuario atualizado".to_string());
    facade.update(&updated_model);

    facade.remove("1");
}